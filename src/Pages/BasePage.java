package Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;


public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    // Constructor
    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.wait = wait;
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void sendKeys(WebElement element,String value){
        this.wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(value);
    }

    public void click(WebElement element){this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public void OpenNewTab(String url) {
        JavascriptExecutor js = (JavascriptExecutor)this.driver;
        js.executeScript("window.open('" + url + "');");
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size()-1));
  }

    public void switchToParentTab() {
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
    }

    public void switchFrames(){
        this.driver.switchTo().frame(1);
    }

    public void switchToParentFrame(){
        this.driver.switchTo().parentFrame();
    }

    public void acceptAlert(){
        this.driver.switchTo().alert().accept();
    }

    public void dismissAlert(){
        this.driver.switchTo().alert().dismiss();
    }
}


