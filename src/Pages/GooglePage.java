package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class GooglePage extends BasePage{

    @FindBy(name = "q")
    private WebElement SEARCH_FIELD;

    @FindBy(className = "gNO89b")
    private List<WebElement> SEARCH_BUTTON;

    @FindBy(css = "[href='https://www.extentreports.com/faq/']")
    private List<WebElement>  faq;

    @FindBy(css = ".LC20lb.DKV0Md span")
    private  List<WebElement> results;

    public GooglePage(WebDriver driver, WebDriverWait wait){
        super(driver,wait);
    }

    public void set_field(String value) {
        this.sendKeys(this.SEARCH_FIELD, value);
    }

    public void search(){
        this.click(this.SEARCH_BUTTON.get(0));
    }

    public void clickFaq(){
        this.click(this.faq.get(1));
    }

    public void clickFirstResult(){
        this.click(results.get(0));
    }
}
