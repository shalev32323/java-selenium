package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    {
        System.setProperty("webdriver.chrome.driver", "/Users/shalevzrihen/Documents/selenium/chromedriver");
    }

    static final String BASE_URL = "https://www.google.com/";
    public static WebDriver driver;
    static WebDriverWait wait;


    @BeforeTest
    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 20);
        driver.get(BASE_URL);
        driver.manage().window().maximize();
    }

    @AfterTest
    public void terminate() {
        driver.quit();
    }
}

