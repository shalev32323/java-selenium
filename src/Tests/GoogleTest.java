package Tests;

import Pages.BasePage;
import Pages.GooglePage;
import org.testng.annotations.Test;

public class GoogleTest extends BaseTest{

    final String VALUE = "selenium";

    @Test
    private void google_test(){
        GooglePage gp = new GooglePage(driver,wait);
        BasePage bp = new GooglePage(driver,wait);
        gp.set_field(this.VALUE);
        gp.search();
        bp.OpenNewTab("https://www.extentreports.com/");
        gp.clickFaq();
        gp.search();
        driver.close();
        bp.switchToParentTab();
        gp.clickFirstResult();
    }

}
