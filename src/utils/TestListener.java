package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import reports.ScreenShotGenerate;


public class TestListener implements ITestListener {

    private ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir") + "/src/reports/Report.html");
    private ExtentReports extent = new ExtentReports();
    public static ExtentTest test;

    @Override
    public void onTestStart(ITestResult result) {
        test = extent.createTest(result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        test.pass(result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
            test.fail(result.getThrowable(),
                    MediaEntityBuilder.createScreenCaptureFromPath("./screenshots/"
                            + ScreenShotGenerate.takeScreenShot(result.getName() + "_")).build());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        test.skip(result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
        spark.config().setDocumentTitle("Report");
        spark.config().setReportName("shalev");
        spark.config().setTheme(Theme.DARK);
        extent.attachReporter(spark);
    }

    @Override
    public void onFinish(ITestContext context) {
        extent.flush();
    }
}

