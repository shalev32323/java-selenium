package reports;

import Tests.BaseTest;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenShotGenerate extends BaseTest {

    public static String takeScreenShot(String screenShotName)  {
        String df = new SimpleDateFormat("yyyyMMddhhss").format(new Date());
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String name = screenShotName + df + ".png";
        String path = System.getProperty("user.dir") + "/src/reports/screenshots/" + name;
        System.out.println(path);
        File destination = new File(path);
        try {
            FileUtils.copyFile(source, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return name;
    }
}
